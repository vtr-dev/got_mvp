package com.softdesign.middleandroidtest.mvp.presenters;

import android.support.annotation.Nullable;

import com.softdesign.middleandroidtest.mvp.views.ISplashView;

public interface ISplashPresenter {

    void takeView(ISplashView splashView);
    void dropView();
    void initView();

    @Nullable
    ISplashView getView();

    void processInitData();

    void onInitError(String msg);
    void onInitComplete();
}

package com.softdesign.middleandroidtest.mvp.presenters;

import android.content.Context;
import android.support.annotation.Nullable;

import com.softdesign.middleandroidtest.R;
import com.softdesign.middleandroidtest.data.storage.models.Character;
import com.softdesign.middleandroidtest.data.storage.models.CharacterDTO;
import com.softdesign.middleandroidtest.mvp.models.CharactersModel;
import com.softdesign.middleandroidtest.mvp.views.ICharacterView;

public class CharacterPresenter implements ICharacterPresenter {

    private static CharacterPresenter ourInstance = new CharacterPresenter();

    private CharactersModel mCharactersModel;
    private ICharacterView mCharacterView;

    private CharacterPresenter() {
        mCharactersModel = new CharactersModel();
    }

    public static CharacterPresenter getInstance() {
        return ourInstance;
    }

    @Override
    public void takeView(ICharacterView characterView) {
        mCharacterView = characterView;
    }

    @Override
    public void dropView() {
        mCharacterView = null;
    }

    @Override
    public void initView() {
    }

    @Nullable
    @Override
    public ICharacterView getView() {
        return mCharacterView;
    }

    @Override
    public void clickOnParent(Character parent) {
        if (getView() != null) {
            getView().showCharacterScreen(parent);
        }
    }

    @Override
    public void checkCharacterDied(CharacterDTO characterDTO) {
        if (getView() != null) {
            if (!characterDTO.getDied().isEmpty()) {
                String message;
                if (!characterDTO.getLastSeason().isEmpty()) {
                    message = String.format(((Context)getView()).getString(
                            R.string.msg_character_died_in_season), characterDTO.getLastSeason());
                } else {
                    message = ((Context)getView()).getString(R.string.msg_character_died);
                }
                getView().showMessage(message);
            }
        }
    }

    @Override
    public void checkFather(CharacterDTO characterDTO) {
        if (getView() != null) {
            int fatherId = characterDTO.getFatherId();
            if (fatherId >= 0) {
                Character father = mCharactersModel.getCharacterById(fatherId);
                if (father != null) {
                    getView().showFatherButton(father);
                }
            }
        }
    }

    @Override
    public void checkMother(CharacterDTO characterDTO) {
        if (getView() != null) {
            int motherId = characterDTO.getMotherId();
            if (motherId >= 0) {
                Character mother = mCharactersModel.getCharacterById(motherId);
                if (mother != null) {
                    getView().showMotherButton(mother);
                }
            }
        }
    }

}

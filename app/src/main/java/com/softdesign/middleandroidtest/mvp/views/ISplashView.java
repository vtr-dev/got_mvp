package com.softdesign.middleandroidtest.mvp.views;

import com.softdesign.middleandroidtest.mvp.presenters.ISplashPresenter;

public interface ISplashView {

    ISplashPresenter getPresenter();

    void showMessage(String message);
    void showError(Throwable e);

    void startAfterDelay(int delay);

    void showLoad();
    void hideLoad();

    void showCharacterListScreen();
}

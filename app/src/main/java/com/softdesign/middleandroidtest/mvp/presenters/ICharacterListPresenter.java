package com.softdesign.middleandroidtest.mvp.presenters;

import android.support.annotation.Nullable;

import com.softdesign.middleandroidtest.data.storage.models.Character;
import com.softdesign.middleandroidtest.mvp.views.ICharacterListView;

import java.util.List;

public interface ICharacterListPresenter {

    void takeView(ICharacterListView characterListView);
    void dropView();
    void initView();

    @Nullable
    ICharacterListView getView();

    void clickOnItem(int pos);

    List<Character> getCharactersByHouseNum(int pos);
}

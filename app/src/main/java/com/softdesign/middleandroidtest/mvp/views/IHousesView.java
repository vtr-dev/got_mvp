package com.softdesign.middleandroidtest.mvp.views;

import com.softdesign.middleandroidtest.mvp.presenters.IHousesPresenter;

public interface IHousesView {

    IHousesPresenter getPresenter();

    void showMessage(String message);
    void showError(Throwable e);

    void updateDrawer(int menuId, int iconId, String drawerTitle);
    void updateTabs(int houseNum);
}

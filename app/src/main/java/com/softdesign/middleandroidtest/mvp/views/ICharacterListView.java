package com.softdesign.middleandroidtest.mvp.views;

import com.softdesign.middleandroidtest.data.storage.models.Character;
import com.softdesign.middleandroidtest.mvp.presenters.ICharacterListPresenter;

public interface ICharacterListView {

    ICharacterListPresenter getPresenter();

    void showError(Throwable e);

    void showCharacterScreen(int characterPos);
}

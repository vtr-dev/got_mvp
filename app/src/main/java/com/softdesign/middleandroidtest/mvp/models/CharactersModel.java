package com.softdesign.middleandroidtest.mvp.models;

import com.softdesign.middleandroidtest.data.managers.DataManager;
import com.softdesign.middleandroidtest.data.storage.models.Character;

import java.util.List;

public class CharactersModel {

    public CharactersModel() {
    }

    public List<Character> getCharactersByHouseId(int houseId) {
        return DataManager.getInstance().getCharactersByHouseFromDb(houseId);
    }

    public Character getCharacterById(int id) {
        return DataManager.getInstance().getCharacterFromDb(id);
    }

}

package com.softdesign.middleandroidtest.mvp.models;

import android.content.Context;
import android.util.Log;

import com.softdesign.middleandroidtest.R;
import com.softdesign.middleandroidtest.data.managers.DataManager;
import com.softdesign.middleandroidtest.data.network.res.CharacterModelRes;
import com.softdesign.middleandroidtest.data.network.res.HouseModelRes;
import com.softdesign.middleandroidtest.data.storage.models.Character;
import com.softdesign.middleandroidtest.data.storage.models.CharacterDao;
import com.softdesign.middleandroidtest.data.storage.models.HouseMember;
import com.softdesign.middleandroidtest.data.storage.models.HouseMemberDao;
import com.softdesign.middleandroidtest.mvp.presenters.SplashPresenter;
import com.softdesign.middleandroidtest.utils.ConstantManager;
import com.softdesign.middleandroidtest.utils.StrUtils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InitDataModel {

    private static final String TAG = ConstantManager.TAG_PREFIX + "InitDataModel";

    private SplashPresenter mSplashPresenter;

    private DataManager mDataManager;
    private CharacterDao mCharacterDao;
    private HouseMemberDao mHouseMemberDao;
    private Context mContext;


    public InitDataModel(SplashPresenter presenter) {
        mSplashPresenter = presenter;
        mDataManager = DataManager.getInstance();
        mCharacterDao = mDataManager.getDaoSession().getCharacterDao();
        mHouseMemberDao = mDataManager.getDaoSession().getHouseMemberDao();
        mContext = mDataManager.getContext();
    }

    public boolean isDataLoaded() {
        return mDataManager.getPreferencesManager().getDataLoadedFlag();
    }

    public void setDataLoadedFlag(boolean flag) {
        mDataManager.getPreferencesManager().saveDataLoadedFlag(flag);
    }

    public void loadAppData() {
        loadHouses(0);
    }

    private void loadHouses(final int houseNum) {
        Call<HouseModelRes> call = mDataManager
                .getHouseModelFromNetwork(ConstantManager.HOUSE_IDS[houseNum]);
        call.enqueue(new Callback<HouseModelRes>() {
            @Override
            public void onResponse(Call<HouseModelRes> call, Response<HouseModelRes> response) {
                if (response.code() == 200) {
                    List<HouseMember> allHouseMembers = new ArrayList<>();
                    for (String memberUrl : response.body().getSwornMembers()) {
                        allHouseMembers.add(new HouseMember(
                                Integer.valueOf(StrUtils.getLastPartOfUrl(memberUrl)),
                                ConstantManager.HOUSE_IDS[houseNum]));
                    }
                    mHouseMemberDao.insertOrReplaceInTx(allHouseMembers);
                    if (houseNum < ConstantManager.HOUSE_IDS.length - 1) {
                        loadHouses(houseNum + 1);
                    }  else {
                        loadCharactersUpPage(1, 50);
                    }
                } else {
                    mSplashPresenter.onInitError(mContext.getString(R.string.msg_data_loading_error));
                    Log.e(TAG, "onResponse: " + String.valueOf(response.errorBody().source()));
                }
            }

            @Override
            public void onFailure(Call<HouseModelRes> call, Throwable t) {
                mSplashPresenter.onInitError(mContext.getString(R.string.msg_network_error));
                Log.e(TAG, "Network error: " + t.getMessage());
            }
        });
    }

    private void loadCharactersUpPage(final int page, final int pageSize) {
        Call<List<CharacterModelRes>> call = mDataManager.getCharactersByPageFromNetwork(page, pageSize);
        call.enqueue(new Callback<List<CharacterModelRes>>() {
            @Override
            public void onResponse(Call<List<CharacterModelRes>> call, Response<List<CharacterModelRes>> response) {
                if (response.code() == 200) {
                    if (!response.body().isEmpty()) {
                        List<Character> allCharacters = new ArrayList<>();
                        for (CharacterModelRes characterModel : response.body()) {
                            allCharacters.add(new Character(characterModel));
                        }
                        mCharacterDao.insertOrReplaceInTx(allCharacters);
                        loadCharactersUpPage(page + 1, pageSize);
                    } else {
                        mSplashPresenter.onInitComplete();
                    }
                } else {
                    mSplashPresenter.onInitError(mContext.getString(R.string.msg_data_loading_error));
                    Log.e(TAG, "onResponse: " + String.valueOf(response.errorBody().source()));
                }
            }

            @Override
            public void onFailure(Call<List<CharacterModelRes>> call, Throwable t) {
                mSplashPresenter.onInitError(mContext.getString(R.string.msg_network_error));
                Log.e(TAG, "Network error: " + t.getMessage());
            }
        });
    }

}

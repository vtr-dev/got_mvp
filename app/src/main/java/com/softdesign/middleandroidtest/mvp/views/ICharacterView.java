package com.softdesign.middleandroidtest.mvp.views;

import com.softdesign.middleandroidtest.data.storage.models.Character;
import com.softdesign.middleandroidtest.mvp.presenters.ICharacterPresenter;

public interface ICharacterView {

    ICharacterPresenter getPresenter();

    void showMessage(String message);
    void showError(Throwable e);

    void showFatherButton(Character father);
    void showMotherButton(Character mother);

    void showCharacterScreen(Character character);
}

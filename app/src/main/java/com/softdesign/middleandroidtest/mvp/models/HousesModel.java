package com.softdesign.middleandroidtest.mvp.models;

import com.softdesign.middleandroidtest.utils.ConstantManager;

public class HousesModel {

    public HousesModel() {
    }

    public String getHouseTitleByNum(int houseNum) {
        return ConstantManager.HOUSE_NAMES[houseNum];
    }

    public int getHouseIdByNum(int houseNum) {
        return ConstantManager.HOUSE_IDS[houseNum];
    }

    public int getHousesCount() {
        return ConstantManager.HOUSE_NAMES.length;
    }

    public int getHouseMenuIdByNum(int houseNum) {
        return ConstantManager.HOUSE_MENU_ITEMS[houseNum];
    }

    public int getHousePosByMenuId(int menuId) {
        for (int i = 0; i < ConstantManager.HOUSE_MENU_ITEMS.length; i++) {
            if (ConstantManager.HOUSE_MENU_ITEMS[i] == menuId) {
                return i;
            }
        }
        return 0;
    }

    public int getHouseIconIdByNum(int houseNum) {
        return ConstantManager.HOUSE_ICONS[houseNum];
    }
}


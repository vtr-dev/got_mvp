package com.softdesign.middleandroidtest.mvp.presenters;

import android.support.annotation.Nullable;

import com.softdesign.middleandroidtest.mvp.views.IHousesView;

public interface IHousesPresenter {

    void takeView(IHousesView housesView);
    void dropView();
    void initView();

    @Nullable
    IHousesView getView();

    void clickOnHouseTab(int pos);
    void clickOnHouseMenu(int menuId);

    int getHousesCount();
    int getHouseId(int pos);
    String getHouseTitle(int pos);
}

package com.softdesign.middleandroidtest.mvp.presenters;

import android.support.annotation.Nullable;

import com.softdesign.middleandroidtest.data.storage.models.Character;
import com.softdesign.middleandroidtest.data.storage.models.CharacterDTO;
import com.softdesign.middleandroidtest.mvp.views.ICharacterView;

public interface ICharacterPresenter {

    void takeView(ICharacterView characterView);
    void dropView();
    void initView();

    @Nullable
    ICharacterView getView();

    void clickOnParent(Character parent);

    void checkCharacterDied(CharacterDTO characterDTO);

    void checkFather(CharacterDTO characterDTO);
    void checkMother(CharacterDTO characterDTO);
}

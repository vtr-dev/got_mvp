package com.softdesign.middleandroidtest.mvp.presenters;

import android.content.Context;
import android.support.annotation.Nullable;

import com.softdesign.middleandroidtest.R;
import com.softdesign.middleandroidtest.mvp.models.InitDataModel;
import com.softdesign.middleandroidtest.mvp.views.ISplashView;
import com.softdesign.middleandroidtest.utils.AppConfig;

public class SplashPresenter implements ISplashPresenter {

    private static SplashPresenter ourInstance = new SplashPresenter();

    private InitDataModel mInitDataModel;
    private ISplashView mSplashView;

    private SplashPresenter() {
        mInitDataModel = new InitDataModel(this);
    }

    public static SplashPresenter getInstance() {
        return ourInstance;
    }

    @Override
    public void takeView(ISplashView splashView) {
        mSplashView = splashView;
    }

    @Override
    public void dropView() {
        mSplashView = null;
    }

    @Override
    public void initView() {
        if (getView() != null) {
            getView().startAfterDelay(AppConfig.SPLASH_DELAY);
        }
    }

    @Nullable
    @Override
    public ISplashView getView() {
        return mSplashView;
    }

    @Override
    public void processInitData() {
        if (getView() != null) {
            if (mInitDataModel.isDataLoaded()) {
                getView().showCharacterListScreen();
            } else {
                if (isNetworkAvailable()) {
                    getView().showLoad();
                    mInitDataModel.loadAppData();
                } else {
                    getView().showMessage(((Context)getView())
                            .getString(R.string.msg_network_isnt_available));
                }
            }
        }
    }

    private boolean isNetworkAvailable() {
        // TODO: 22.10.2016
        return true;
    }

    @Override
    public void onInitError(String msg) {
        if (getView() != null) {
            getView().hideLoad();
            getView().showMessage(msg);
        }
    }

    @Override
    public void onInitComplete() {
        if (getView() != null) {
            getView().hideLoad();
            mInitDataModel.setDataLoadedFlag(true);
            getView().showCharacterListScreen();
        }
    }

}

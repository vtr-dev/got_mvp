package com.softdesign.middleandroidtest.mvp.presenters;

import android.support.annotation.Nullable;

import com.softdesign.middleandroidtest.data.storage.models.Character;
import com.softdesign.middleandroidtest.mvp.models.CharactersModel;
import com.softdesign.middleandroidtest.mvp.models.HousesModel;
import com.softdesign.middleandroidtest.mvp.views.ICharacterListView;
import com.softdesign.middleandroidtest.utils.AppConfig;

import java.util.List;

public class CharacterListPresenter implements ICharacterListPresenter {

    private static CharacterListPresenter[] ourInstances =
            new CharacterListPresenter[AppConfig.HOUSES_COUNT];

    private CharactersModel mCharactersModel;
    private HousesModel mHousesModel;
    private ICharacterListView mCharacterListView;

    private CharacterListPresenter() {
        mCharactersModel = new CharactersModel();
        mHousesModel = new HousesModel();
    }

    public static CharacterListPresenter getInstance(int houseNum) {
        if (ourInstances[houseNum] == null) {
            ourInstances[houseNum] = new CharacterListPresenter();
        }
        return ourInstances[houseNum];
    }

    @Override
    public void takeView(ICharacterListView characterListView) {
        mCharacterListView = characterListView;
    }

    @Override
    public void dropView() {
        mCharacterListView = null;
    }

    @Override
    public void initView() {
    }

    @Nullable
    @Override
    public ICharacterListView getView() {
        return mCharacterListView;
    }

    @Override
    public void clickOnItem(int pos) {
        if (getView() != null) {
            getView().showCharacterScreen(pos);
        }
    }

    @Override
    public List<Character> getCharactersByHouseNum(int pos) {
        int houseId = mHousesModel.getHouseIdByNum(pos);
        return mCharactersModel.getCharactersByHouseId(houseId);
    }
}

package com.softdesign.middleandroidtest.mvp.presenters;

import android.support.annotation.Nullable;

import com.softdesign.middleandroidtest.mvp.models.HousesModel;
import com.softdesign.middleandroidtest.mvp.views.IHousesView;

public class HousesPresenter implements IHousesPresenter {

    private static HousesPresenter ourInstance = new HousesPresenter();

    private HousesModel mHousesModel;
    private IHousesView mHousesView;

    private HousesPresenter() {
        mHousesModel = new HousesModel();
    }

    public static HousesPresenter getInstance() {
        return ourInstance;
    }

    @Override
    public void takeView(IHousesView housesView) {
        mHousesView = housesView;
    }

    @Override
    public void dropView() {
        mHousesView = null;
    }

    @Override
    public void initView() {
    }

    @Nullable
    @Override
    public IHousesView getView() {
        return mHousesView;
    }

    @Override
    public void clickOnHouseTab(int pos) {
        if (getView() != null) {
            getView().updateDrawer(
                    mHousesModel.getHouseMenuIdByNum(pos),
                    mHousesModel.getHouseIconIdByNum(pos),
                    mHousesModel.getHouseTitleByNum(pos)
            );
        }
    }

    @Override
    public void clickOnHouseMenu(int menuId) {
        if (getView() != null) {
            getView().updateTabs(mHousesModel.getHousePosByMenuId(menuId));
        }
    }

    @Override
    public int getHousesCount() {
        return mHousesModel.getHousesCount();
    }

    @Override
    public int getHouseId(int pos) {
        return mHousesModel.getHouseIdByNum(pos);
    }

    @Override
    public String getHouseTitle(int pos) {
        return mHousesModel.getHouseTitleByNum(pos);
    }
}

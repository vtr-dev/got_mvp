package com.softdesign.middleandroidtest.ui.activities;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;

import com.softdesign.middleandroidtest.R;
import com.softdesign.middleandroidtest.mvp.presenters.ISplashPresenter;
import com.softdesign.middleandroidtest.mvp.presenters.SplashPresenter;
import com.softdesign.middleandroidtest.mvp.views.ISplashView;

public class SplashActivity extends BaseActivity implements ISplashView {

    private SplashPresenter mPresenter = SplashPresenter.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        mPresenter.takeView(this);
        mPresenter.initView();
    }

    @Override
    protected void onDestroy() {
        mPresenter.dropView();
        super.onDestroy();
    }

    //region ================== ISplashView ==================

    @Override
    public void startAfterDelay(int delay) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mPresenter.processInitData();
            }
        }, delay);
    }

    @Override
    public void showCharacterListScreen() {
        startActivity(new Intent(SplashActivity.this, CharacterListActivity.class));
    }

    @Override
    public ISplashPresenter getPresenter() {
        return mPresenter;
    }

    @Override
    public void showMessage(String message) {
        showToast(message);
    }

    @Override
    public void showError(Throwable e) {
        showError(e.getMessage(), (Exception) e);
    }

    @Override
    public void showLoad() {
        showProgress();
    }

    @Override
    public void hideLoad() {
        hideProgress();
    }

    //endregion ================== ISplashView ==================

}

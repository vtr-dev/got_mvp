package com.softdesign.middleandroidtest.ui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.softdesign.middleandroidtest.R;
import com.softdesign.middleandroidtest.data.managers.DataManager;
import com.softdesign.middleandroidtest.data.storage.models.Character;
import com.softdesign.middleandroidtest.data.storage.models.CharacterDTO;
import com.softdesign.middleandroidtest.mvp.presenters.CharacterListPresenter;
import com.softdesign.middleandroidtest.mvp.presenters.ICharacterListPresenter;
import com.softdesign.middleandroidtest.mvp.views.ICharacterListView;
import com.softdesign.middleandroidtest.ui.activities.CharacterActivity;
import com.softdesign.middleandroidtest.ui.adapters.CharactersAdapter;
import com.softdesign.middleandroidtest.ui.views.SimpleDividerItemDecoration;
import com.softdesign.middleandroidtest.utils.ConstantManager;

import java.util.List;

public class HouseMemberListFragment extends Fragment implements ICharacterListView {

    private static final String TAG = ConstantManager.TAG_PREFIX + "CharListFragment";

    private CharacterListPresenter mPresenter;

    private static final String ARG_POSITION = "POSITION";

    private List<Character> mHouseMembers;


    public HouseMemberListFragment() {
    }

    public static HouseMemberListFragment newInstance(int position) {
        HouseMemberListFragment fragment = new HouseMemberListFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_POSITION, position);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_house_member_list, container, false);

        RecyclerView charactersRecyclerView = (RecyclerView) rootView
                .findViewById(R.id.characters_recycler_view);
        charactersRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        charactersRecyclerView.addItemDecoration(new SimpleDividerItemDecoration(getContext()));

        CharactersAdapter charactersAdapter =
                new CharactersAdapter(mHouseMembers, getArguments().getInt(ARG_POSITION),
                        new CharactersAdapter.CharacterViewHolder.CustomClickListener() {
                            @Override
                            public void onCharacterItemClickListener(int position) {
                                mPresenter.clickOnItem(position);
                            }
                        }
                );
        charactersRecyclerView.setAdapter(charactersAdapter);

        return rootView;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

        mPresenter =  CharacterListPresenter.getInstance(getArguments().getInt(ARG_POSITION));

        mPresenter.takeView(this);
        mPresenter.initView();

        mHouseMembers = mPresenter.getCharactersByHouseNum(getArguments().getInt(ARG_POSITION));
    }

    @Override
    public void onDestroy() {
        mPresenter.dropView();
        super.onDestroy();
    }

    @Override
    public ICharacterListPresenter getPresenter() {
        return mPresenter;
    }

    @Override
    public void showError(Throwable e) {
        Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
        Log.e(TAG, e.toString());
    }

    @Override
    public void showCharacterScreen(int characterPos) {
        CharacterDTO characterDTO = new CharacterDTO(
                getArguments().getInt(ARG_POSITION), mHouseMembers.get(characterPos));
        Intent profileIntent = new Intent(getActivity(), CharacterActivity.class);
        profileIntent.putExtra(ConstantManager.PARCELABLE_KEY, characterDTO);
        startActivity(profileIntent);
    }
}

package com.softdesign.middleandroidtest.ui.activities;

import android.content.Intent;
import android.os.PersistableBundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.softdesign.middleandroidtest.R;
import com.softdesign.middleandroidtest.data.storage.models.Character;
import com.softdesign.middleandroidtest.data.storage.models.CharacterDTO;
import com.softdesign.middleandroidtest.mvp.presenters.CharacterPresenter;
import com.softdesign.middleandroidtest.mvp.presenters.ICharacterPresenter;
import com.softdesign.middleandroidtest.mvp.views.ICharacterView;
import com.softdesign.middleandroidtest.utils.ConstantManager;

public class CharacterActivity extends AppCompatActivity implements ICharacterView {

    private static final String TAG = ConstantManager.TAG_PREFIX + "CharacterActivity";

    private CharacterPresenter mPresenter = CharacterPresenter.getInstance();

    private Toolbar mToolbar;
    private CollapsingToolbarLayout mCollapsingToolbarLayout;
    private CoordinatorLayout mCoordinatorLayout;

    private ImageView mHouseImage;
    private TextView mWordsTextView, mBornTextView, mDiedTextView, mTitlesTextView, mAliasesTextView;
    private Button mFatherButton, mMotherButton;
    private TableRow mFatherRow, mMotherRow;

    private int mHouseNum;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_character);

        mHouseImage = (ImageView) findViewById(R.id.house_img);
        mWordsTextView = (TextView) findViewById(R.id.words_tv);
        mBornTextView = (TextView) findViewById(R.id.born_tv);
        mDiedTextView = (TextView) findViewById(R.id.died_tv);
        mTitlesTextView = (TextView) findViewById(R.id.titles_tv);
        mAliasesTextView = (TextView) findViewById(R.id.aliases_tv);
        mFatherButton = (Button) findViewById(R.id.father_btn);
        mMotherButton = (Button) findViewById(R.id.mother_btn);
        mFatherRow = (TableRow) findViewById(R.id.father_row);
        mMotherRow = (TableRow) findViewById(R.id.mother_row);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mCollapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        mCoordinatorLayout = (CoordinatorLayout) findViewById(R.id.main_coordinator_container);

        mPresenter.takeView(this);
        mPresenter.initView();

        initCharacterData();

        setupToolbar();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void setupToolbar() {
        setSupportActionBar(mToolbar);

        ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    private void initCharacterData() {
        CharacterDTO characterDTO = getIntent().getParcelableExtra(ConstantManager.PARCELABLE_KEY);
        mHouseNum = characterDTO.getHouseNum();

        mHouseImage.setImageResource(ConstantManager.HOUSE_POSTERS[mHouseNum]);
        mCollapsingToolbarLayout.setTitle(characterDTO.getName());

        mWordsTextView.setText(ConstantManager.HOUSE_WORDS[mHouseNum]);
        mBornTextView.setText(characterDTO.getBorn());
        mDiedTextView.setText(characterDTO.getDied());
        mTitlesTextView.setText(characterDTO.getTitles());
        mAliasesTextView.setText(characterDTO.getAliases());

        mPresenter.checkCharacterDied(characterDTO);

        mFatherRow.setVisibility(View.GONE);
        mPresenter.checkFather(characterDTO);

        mMotherRow.setVisibility(View.GONE);
        mPresenter.checkMother(characterDTO);
    }

    //region ================== ICharacterView ==================

    @Override
    public ICharacterPresenter getPresenter() {
        return mPresenter;
    }

    @Override
    public void showMessage(String message) {
        Snackbar.make(mCoordinatorLayout, message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showError(Throwable e) {
        Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
        Log.e(TAG, e.toString());
    }

    @Override
    public void showFatherButton(final Character father) {
        mFatherRow.setVisibility(View.VISIBLE);
        mFatherButton.setText(father.getName());
        mFatherButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.clickOnParent(father);
            }
        });
    }

    @Override
    public void showMotherButton(final Character mother) {
        mMotherRow.setVisibility(View.VISIBLE);
        mMotherButton.setText(mother.getName());
        mMotherButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.clickOnParent(mother);
            }
        });
    }

    @Override
    public void showCharacterScreen(Character character) {
        CharacterDTO characterDTO = new CharacterDTO(mHouseNum, character);
        Intent profileIntent = new Intent(CharacterActivity.this, CharacterActivity.class);
        profileIntent.putExtra(ConstantManager.PARCELABLE_KEY, characterDTO);
        startActivity(profileIntent);
    }

    //endregion ================== ICharacterView ==================
}

package com.softdesign.middleandroidtest.ui.activities;

import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.softdesign.middleandroidtest.R;
import com.softdesign.middleandroidtest.mvp.presenters.HousesPresenter;
import com.softdesign.middleandroidtest.mvp.presenters.IHousesPresenter;
import com.softdesign.middleandroidtest.mvp.views.IHousesView;
import com.softdesign.middleandroidtest.ui.adapters.HousesPagerAdapter;
import com.softdesign.middleandroidtest.utils.ConstantManager;

public class CharacterListActivity extends AppCompatActivity implements IHousesView {

    private static final String TAG = ConstantManager.TAG_PREFIX + "CharListActivity";

    private HousesPresenter mPresenter = HousesPresenter.getInstance();

    private Toolbar mToolbar;
    private DrawerLayout mNavigationDrawer;
    private NavigationView mNavigationView;
    private CoordinatorLayout mCoordinatorLayout;

    private HousesPagerAdapter mHousesPagerAdapter;

    private ViewPager mViewPager;
    private TabLayout mTabLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_character_list);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mNavigationDrawer = (DrawerLayout) findViewById(R.id.navigation_drawer);
        mNavigationView = (NavigationView) findViewById(R.id.navigation_view);
        mCoordinatorLayout = (CoordinatorLayout) findViewById(R.id.main_coordinator_container);

        setupToolbar();

        mPresenter.takeView(this);
        mPresenter.initView();

        mHousesPagerAdapter = new HousesPagerAdapter(getSupportFragmentManager());

        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mHousesPagerAdapter);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }
            @Override
            public void onPageSelected(int position) {
                mPresenter.clickOnHouseTab(position);
            }
            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        mTabLayout = (TabLayout) findViewById(R.id.tabs);
        mTabLayout.setupWithViewPager(mViewPager);

        setupDrawer();
    }

    @Override
    protected void onDestroy() {
        mPresenter.dropView();
        super.onDestroy();
    }

    private void setupToolbar() {
        setSupportActionBar(mToolbar);

        ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setHomeAsUpIndicator(R.drawable.ic_menu_white_24dp);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            mNavigationDrawer.openDrawer(GravityCompat.START);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (mNavigationDrawer != null && mNavigationDrawer.isDrawerOpen(GravityCompat.START)) {
            mNavigationDrawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    private void setupDrawer() {
        if (mNavigationView != null) {
            mNavigationView.setNavigationItemSelectedListener(
                    new NavigationView.OnNavigationItemSelectedListener() {
                        @Override
                        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                            mPresenter.clickOnHouseMenu(item.getItemId());
                            return false;
                        }
                    }
            );
        }
    }

    //region ================== IHousesView ==================

    @Override
    public IHousesPresenter getPresenter() {
        return mPresenter;
    }

    @Override
    public void showMessage(String message) {
        Snackbar.make(mCoordinatorLayout, message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showError(Throwable e) {
        Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
        Log.e(TAG, e.toString());
    }

    @Override
    public void updateDrawer(int menuId, int iconId, String drawerTitle) {
        if (mNavigationView != null) {
            View headerView = mNavigationView.getHeaderView(0);
            ((ImageView)headerView.findViewById(R.id.drawer_avatar)).setImageResource(iconId);
            ((TextView)headerView.findViewById(R.id.drawer_title))
                    .setText(String.format(getString(R.string.drawer_title), drawerTitle));
            mNavigationView.setCheckedItem(menuId);
        }
    }

    @Override
    public void updateTabs(int houseNum) {
        mNavigationDrawer.closeDrawer(GravityCompat.START);
        TabLayout.Tab selTab = mTabLayout.getTabAt(houseNum);
        if (selTab != null) {
            selTab.select();
        }
    }

    //endregion ================== IHousesView ==================

}

package com.softdesign.middleandroidtest.ui.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.softdesign.middleandroidtest.mvp.presenters.HousesPresenter;
import com.softdesign.middleandroidtest.ui.fragments.HouseMemberListFragment;
import com.softdesign.middleandroidtest.utils.ConstantManager;

public class HousesPagerAdapter extends FragmentPagerAdapter {

    private HousesPresenter mPresenter = HousesPresenter.getInstance();

    public HousesPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return HouseMemberListFragment.newInstance(position);
    }

    @Override
    public int getCount() {
        return mPresenter.getHousesCount();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mPresenter.getHouseTitle(position);
    }
}

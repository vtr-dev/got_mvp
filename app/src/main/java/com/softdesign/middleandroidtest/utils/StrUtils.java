package com.softdesign.middleandroidtest.utils;

import java.util.List;

public class StrUtils {

    public static String join(List<String> strList, char separator) {
        StringBuilder strBuilder = new StringBuilder();
        for (int i = 0; i < strList.size(); i++) {
            strBuilder.append(strList.get(i));
            if (i < strList.size() - 1) {
                strBuilder.append(separator);
            }
        }
        return strBuilder.toString();
    }

    public static String getLastPartOfUrl(String url) {
        return url.substring(url.lastIndexOf('/') + 1, url.length());
    }

}

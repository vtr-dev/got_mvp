package com.softdesign.middleandroidtest.data.managers;

import android.content.SharedPreferences;

import com.softdesign.middleandroidtest.utils.ConstantManager;
import com.softdesign.middleandroidtest.utils.MiddleAndroidTestApplication;

public class PreferencesManager {

    private SharedPreferences mSharedPreferences;


    public PreferencesManager() {
        this.mSharedPreferences = MiddleAndroidTestApplication.getSharedPreferences();
    }


    /**
     * Сохраняет флаг загрузки и сохранения данных в Shared Preferences
     * @param flag флаг
     */
    public void saveDataLoadedFlag(boolean flag) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putBoolean(ConstantManager.IS_DATA_LOADED_KEY, flag);
        editor.apply();
    }

    /**
     * Считывает флаг загрузки и сохранения данных из Shared Preferences
     * @return флаг
     */
    public boolean getDataLoadedFlag() {
        return mSharedPreferences.getBoolean(ConstantManager.IS_DATA_LOADED_KEY, false);
    }
}

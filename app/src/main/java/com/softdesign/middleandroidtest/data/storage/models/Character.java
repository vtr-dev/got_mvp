package com.softdesign.middleandroidtest.data.storage.models;

import com.softdesign.middleandroidtest.data.network.res.CharacterModelRes;
import com.softdesign.middleandroidtest.utils.StrUtils;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.Unique;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.DaoException;

@Entity(active = true, nameInDb = "CHARACTERS")
public class Character {

    @Id
    private Long id;

    @NotNull
    @Unique
    private Integer character_id;

    private String name;

    private String words;

    private String born;

    private String died;

    private String titles;

    private String aliases;

    private Integer father_id;

    private Integer mother_id;

    private String last_season;

    /** Used for active entity operations. */
    @Generated(hash = 898307126)
    private transient CharacterDao myDao;

    /** Used to resolve relations */
    @Generated(hash = 2040040024)
    private transient DaoSession daoSession;

    public Character(CharacterModelRes characterModelRes) {
        this.character_id = Integer.valueOf(StrUtils.getLastPartOfUrl(characterModelRes.getUrl()));
        this.name = characterModelRes.getName();
        this.words = "";
        this.born = characterModelRes.getBorn();
        this.died = characterModelRes.getDied();
        this.titles = StrUtils.join(characterModelRes.getTitles(), '\n');
        this.aliases = StrUtils.join(characterModelRes.getAliases(), '\n');
        if (!characterModelRes.getFather().isEmpty()) {
            this.father_id = Integer.valueOf(StrUtils.getLastPartOfUrl(characterModelRes.getFather()));
        } else {
            this.father_id = -1;
        }
        if (!characterModelRes.getMother().isEmpty()) {
            this.mother_id = Integer.valueOf(StrUtils.getLastPartOfUrl(characterModelRes.getMother()));
        } else {
            this.mother_id = -1;
        }
        if (!characterModelRes.getTvSeries().isEmpty()) {
            this.last_season = characterModelRes.getTvSeries()
                    .get(characterModelRes.getTvSeries().size() - 1).toString();
        } else {
            this.last_season = "";
        }
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#refresh(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 1942392019)
    public void refresh() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.refresh(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#update(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 713229351)
    public void update() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.update(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#delete(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 128553479)
    public void delete() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.delete(this);
    }

    /** called by internal mechanisms, do not call yourself. */
    @Generated(hash = 162219484)
    public void __setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
        myDao = daoSession != null ? daoSession.getCharacterDao() : null;
    }

    public String getLast_season() {
        return this.last_season;
    }

    public void setLast_season(String last_season) {
        this.last_season = last_season;
    }

    public Integer getMother_id() {
        return this.mother_id;
    }

    public void setMother_id(Integer mother_id) {
        this.mother_id = mother_id;
    }

    public Integer getFather_id() {
        return this.father_id;
    }

    public void setFather_id(Integer father_id) {
        this.father_id = father_id;
    }

    public String getAliases() {
        return this.aliases;
    }

    public void setAliases(String aliases) {
        this.aliases = aliases;
    }

    public String getTitles() {
        return this.titles;
    }

    public void setTitles(String titles) {
        this.titles = titles;
    }

    public String getDied() {
        return this.died;
    }

    public void setDied(String died) {
        this.died = died;
    }

    public String getBorn() {
        return this.born;
    }

    public void setBorn(String born) {
        this.born = born;
    }

    public String getWords() {
        return this.words;
    }

    public void setWords(String words) {
        this.words = words;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCharacter_id() {
        return this.character_id;
    }

    public void setCharacter_id(Integer character_id) {
        this.character_id = character_id;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Generated(hash = 1401003829)
    public Character(Long id, @NotNull Integer character_id, String name, String words, String born,
            String died, String titles, String aliases, Integer father_id, Integer mother_id,
            String last_season) {
        this.id = id;
        this.character_id = character_id;
        this.name = name;
        this.words = words;
        this.born = born;
        this.died = died;
        this.titles = titles;
        this.aliases = aliases;
        this.father_id = father_id;
        this.mother_id = mother_id;
        this.last_season = last_season;
    }

    @Generated(hash = 1853959157)
    public Character() {
    }
    
}
